package com.example.composepractice

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composepractice.ui.theme.ComposePracticeTheme

@Composable
fun BusinessCard(){
    val image = painterResource(id = R.drawable.ic_baseline_camera_24)
    Column(modifier = Modifier
        .fillMaxWidth()
        .fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box {
            Image(
                painter = image,
                modifier = Modifier.height(20.dp),
                contentDescription = null,
            )

        }
        Column {
            Text(text = "Tyler Gandy",
                modifier = Modifier.padding(top=15.dp, start = 35.dp), fontSize = 20.sp)
            Text(text = "Android Developer", modifier = Modifier.padding(start = 33.dp),
            )

        }
        Column (modifier = Modifier.padding(top = 40.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally){
            RowBusinessCard(painterResource(R.drawable.ic_baseline_local_phone_24),Constants.phoneNumb)
            RowBusinessCard(painterResource(R.drawable.ic_baseline_attach_email_24),Constants.email)
            RowBusinessCard(painterResource(R.drawable.ic_baseline_people_alt_24),Constants.socialMedia)
        }
    }
}
@Composable
fun RowBusinessCard(painter: Painter, contact: String){
    Row(){
        Icon(painter = painter , contentDescription = null )
        Text(text = contact, modifier = Modifier.padding(start = 10.dp))
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreviewD() {
    ComposePracticeTheme {
        BusinessCard()
    }
}
