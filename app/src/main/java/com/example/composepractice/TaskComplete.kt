package com.example.composepractice

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composepractice.ui.theme.ComposePracticeTheme

@Composable
fun TaskCompleteScreen(){
    val image = painterResource(id = R.drawable.ic_task_completed)
    Column(modifier = Modifier
        .fillMaxWidth()
        .fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box {
            Image(
                painter = image,
                contentDescription = null,
                )

        }
        Column {
            Text(text = Constants.completedTask,
            modifier = Modifier.padding(top=15.dp), fontSize = 20.sp)
            Text(text = Constants.niceWork, modifier = Modifier.padding(start = 35.dp),
            )

        }
    }
}



@Preview(showBackground = true)
@Composable
fun DefaultPreviewB() {
    ComposePracticeTheme {
        TaskCompleteScreen()    }
}
